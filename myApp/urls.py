from django.conf.urls import url

from . import views


urlpatterns = [
    # ex: /polls/
    url(r'^$',      views.index,    name='index'),
    url(r'^base/$', views.base,     name='base'),
    url(r'^msj/$',  views.msj,      name='msj'),
    url(r'^form/$', views.form,     name='form'),
    url(r'^form/manage_authors$', views.manage_authors,     name='manage_authors'),
    url(r'^form/manage_suscrito$', views.manage_suscrito,     name='manage_suscrito'),
    url(r'^form/add$', views.add,     name='add'),
    #url(r'^$', views.index2, name='index2'),
	#url(r'^certificado/$', views.index, name='certificado'),
	#url(r'^certificado/(?P<people>[0-9]+)/$', views.certificado, name='certificado123'),
	#url(r'^certificado/(?P<people>[0-9]+)/Activo/$', views.certificado1, name='certificado'),
	#url(r'^certificado/(?P<people>[0-9]+)/Activo/(?P<year>[0-9]+)/$', views.certificado11, name='certificado'),
	#url(r'^certificado/(?P<people>[0-9]+)/Egresado/$', views.certificado2, name='certificado'),
	#url(r'^certificado/(?P<people>[0-9]+)/Notas/$', views.certificado3, name='certificado'),
	#url(r'^certificado/(?P<people>[0-9]+)/Notas/(?P<year>[0-9]+)/$', views.certificado33, name='certificado'),
	#url(r'^load/$', views.load, name='load'),
]
