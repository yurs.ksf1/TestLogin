#from django.shortcuts import render
from django.shortcuts import render
#import loggings

from django.http import HttpResponseRedirect, HttpResponse
# Create your views here.

#logger = logging.getLogger('api')


def index(request):
	msj = "Hello index"
	return render(request, 'msj.html', {'msj': msj,})
	#return HttpResponse(template.render(context, request))


def form(request):
	msj = "Hello form"
	return render(request, 'form.html', {'msj': msj,})
	#return HttpResponse(template.render(context, request))

def msj(request):
	msj = "Hello World"
	return render(request, 'msj.html', {'msj': msj,})
	#return HttpResponse(template.render(context, request))

def base(request):
	msj = "Hello World"
	return render(request, 'base.html', {'msj': msj,})
	#return HttpResponse(template.render(context, request))


from django.forms import modelformset_factory
from django.shortcuts import render
from myApp.models import Author, Suscrito, Book
from myApp.forms import SuscritoForm

def manage_authors(request):
    AuthorFormSet = modelformset_factory(Author, fields=('name', 'title'))
    if request.method == 'POST':
        formset = AuthorFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            # do something.
    else:
        formset = AuthorFormSet()
    return render(request, 'manage_authors.html', {'formset': formset})

def manage_suscrito(request):
    SuscritoFormSet = modelformset_factory(Suscrito, fields=('user', 'email', 'sentMsj', 'birth_date'))
    if request.method == 'POST':
        formset = SuscritoFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            # do something.
    else:
        formset = SuscritoFormSet()
    return render(request, 'manage_authors.html', {'formset': formset})

def add(request):
	form = SuscritoForm(request.POST)
	if request.POST and form.is_valid():
		pass # Do whatever

	return render(request, "add.html", {"form": form})




"""
    SuscritoFormSet = modelformset_factory(Suscrito, fields=('user', 'email', 'sentMsj', 'birth_date'))
    if request.method == 'POST':
        formset = SuscritoFormSet(request.POST, request.FILES)
        if formset.is_valid():
            formset.save()
            # do something.
    else:
        formset = SuscritoFormSet()
    return render(request, 'add.html', {'formset': formset})
"""


"""
def index(request):

    return HttpResponse("Hello, world. ")
"""
