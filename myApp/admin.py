from django.contrib import admin
from .models import Author, Book, Suscrito

#from .models import Author
#from guardian.admin import GuardedModelAdmin

# Old way:
#class AuthorAdmin(admin.ModelAdmin):
#    pass

# With object permissions support


#class AuthorAdmin(GuardedModelAdmin):
#    pass

#admin.site.register(Author, AuthorAdmin)

admin.site.register(Suscrito)
admin.site.register(Author)
admin.site.register(Book)
