from django import forms
from django.db import models
"""
class AuthorForm(forms.Form):
    name = forms.CharField(max_length=100)
    title = forms.CharField(
        max_length=3,
        widget=forms.Select(choices=TITLE_CHOICES),
    )
    birth_date = forms.DateField(required=False)

class BookForm(forms.Form):
    name = forms.CharField(max_length=100)
    authors = forms.ModelMultipleChoiceField(queryset=Author.objects.all())
"""
class SuscritoForm(forms.Form):
    user = models.CharField(max_length=100, unique=True)
    email = models.EmailField(max_length=254, unique=True)
    sentMsj = models.BooleanField(default=True)
    birth_date = models.DateField(blank=True, null=True)
