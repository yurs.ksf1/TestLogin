# -*- coding: utf-8 -*-
# Generated by Django 1.11.3 on 2017-08-01 17:06
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('myApp', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Suscrito',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('user', models.CharField(max_length=100, unique=True)),
                ('email', models.EmailField(max_length=254, unique=True)),
                ('sentMsj', models.BooleanField(default=True)),
                ('birth_date', models.DateField(blank=True, null=True)),
            ],
        ),
    ]
